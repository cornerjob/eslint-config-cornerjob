# eslint-config-cornerjob

[![npm version](https://badge.fury.io/js/eslint-config-cornerjob.svg)](http://badge.fury.io/js/eslint-config-cornerjob)

This package provides CornerJob's .eslintrc as an extensible shared config.

## Installation

The easiest way to install eslint-config-cornerjob is with [`npm`][npm].

[npm]: https://www.npmjs.com/

```sh
npm install eslint-config-cornerjob
```

Alternately, download the source.

```sh
git clone https://bitbucket.org/cornerjob/eslint-config-cornerjob.git
```
